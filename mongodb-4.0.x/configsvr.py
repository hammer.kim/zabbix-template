#!/usr/bin/python
#
from __future__ import print_function
import sys, getopt
from pyzabbix import ZabbixMetric, ZabbixSender
from pymongo import MongoClient
import time
import urllib
import platform
import os

"""
For Config Server
"""

ZBSERVER = '127.0.0.1'
ZBPORT = 10051

os_ = platform.system()

# for item in os.environ:
#    print("%s = %s" % (item, os.environ[item]))

# print(" TEMP = " + os.environ["TEMP"])

# --------------------------------------------------------------------------------------------------
# for hadoop cluster
# --------------------------------------------------------------------------------------------------
# 27017 : mongos
# 27020 : config server
# 27021 : shard server
# mongostat --port 27017 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27020 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27021 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders

# --------------------------------------------------------------------------------------------------
# for nngp cluster
# --------------------------------------------------------------------------------------------------
# 27017 : mongos
# 27022 : config server
# 27033 : shard server
# mongostat --port 27017 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27022 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27033 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders

# E:/GitLab/zabbix-template/mongodb-4.0.x/configsvr.py -h nngp001   -n nngp001 -u admin -s admin -p 27022 -m test
# E:/GitLab/zabbix-template/mongodb-4.0.x/configsvr.py -h hadoop001 -n hadoop001 -u root -s aimiramm -p 27020 -m test

testmode='real'

# t --> 1 : test mode ( Do not send the data to Zabbix Server )
#       0 : real mode ( Send the data to Zabbix Server  )
try:
    opts, args = getopt.getopt(sys.argv[1:],"h:n:p:u:s:m:")
except getopt.GetoptError:
    print ("Usage:\nmongos.py -h <hostname or ip mongod is listening to> -n <hostname in zabbix> -u <user> -s <secret pass> -p <mongod port> -m <test mode>")
    sys.exit(2)

# print(len(sys.argv))
# print(sys.argv[11])
# print(sys.argv[12])

for opt, arg in opts:
    if opt == '-h':
        mongohost = arg
    elif opt == '-n':
        zbhost = arg
    elif opt == '-p':
        mongoport = arg
    elif opt == '-u':
        muser = arg
    elif opt == '-s':
        mpass = arg
    elif opt == '-m':
        testmode = arg

# faced a strange problem - zabbix server add a space character to parameter passed to script
# and spend a hour to catch the bug :(
mongohost = mongohost.rstrip()
zbhost    = zbhost.rstrip()

# print("DEBUG: STR: " + arg)
# print("DEBUG: RES: " + res)
# print("DEBUG: ERR: " + err + str(len(err)))

state = 0

# mongostat --port 27021 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders
# Read saved opcounters from previous check
try:
    if os_ == "Windows":
        temp_dir = os.environ["TEMP"]
        # f = open("C:\\Users\\hammer\\" + mongohost + "-configsvr-opcounters")
        f = open(temp_dir + "\\" + mongohost + "-configsvr-opcounters")
    elif os_ == "Linux":
        f = open("/tmp/" + mongohost + "-configsvr-opcounters")

    s = f.read()
    f.close()
    ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _net_in, _net_out, _flushes = s.split(" ")
except Exception as e:
    ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _net_in, _net_out, _flushes = [1, 0, 0, 0,
                                                                                                            0, 0, 0, 0,
                                                                                                            0, 0, 0]
    print(e)

print("History opcounters")
print(ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _net_in, _net_out, _flushes)

# Get serverStatus stats
try:
    mo = MongoClient('mongodb://' + muser + ':' + urllib.quote(mpass) + '@' + mongohost + ':' + mongoport + '/admin',
                     connectTimeoutMS=5000)
except Exception as e:
    print('Can\'t connect to ' + mongohost)
    print("ERROR:", e)
    sys.exit(1)

res = mo.admin.command('serverStatus')
now = time.time()

insert = int((float(res['opcounters']['insert'])))
query = int((float(res['opcounters']['query'])))
update = int((float(res['opcounters']['update'])))
delete = int((float(res['opcounters']['delete'])))
getmore = int((float(res['opcounters']['getmore'])))
command = int((float(res['opcounters']['command'])))

# check disctionary
if 'opcountersRepl' in res:
    commandRepl = int((float(res['opcountersRepl']['command'])))
else:
    commandRepl = 0;

dirty = round((float(res['wiredTiger']['cache']['tracked dirty bytes in the cache']) / float(
    res['wiredTiger']['cache']['maximum bytes configured'])) * 100, 2)
used = round((float(res['wiredTiger']['cache']['bytes currently in the cache']) / float(
    res['wiredTiger']['cache']['maximum bytes configured'])) * 100, 2)

# print( dirty, used)

flushes = int((float(res['wiredTiger']['transaction']['transaction checkpoints'])))

vsize = int((float(res['mem']['virtual'])))
rsize = int((float(res['mem']['resident'])))

# not in monogos
q_readers = int((float(res['globalLock']['currentQueue']['readers'])))
q_writers = int((float(res['globalLock']['currentQueue']['writers'])))
a_readers = int((float(res['globalLock']['activeClients']['readers'])))
a_writers = int((float(res['globalLock']['activeClients']['writers'])))

net_in = int((float(res['network']['bytesIn'])))
net_out = int((float(res['network']['bytesOut'])))

conn = int((float(res['connections']['current'])))
connAvail = int((float(res['connections']['available'])))

insert_      = int((float(insert)      - float(_insert))      / (now - float(ts)))
query_       = int((float(query)       - float(_query))       / (now - float(ts)))
update_      = int((float(update)      - float(_update))      / (now - float(ts)))
delete_      = int((float(delete)      - float(_delete))      / (now - float(ts)))
getmore_     = int((float(getmore)     - float(_getmore))     / (now - float(ts)))
command_     = int((float(command)     - float(_command))     / (now - float(ts)))
commandRepl_ = int((float(commandRepl) - float(_commandRepl)) / (now - float(ts)))

dirty_ = dirty
used_ = used
flushes_ = int((float(flushes) - float(_flushes)) / ((now - float(ts))))
# unit is Megabytes, so convert to bytes
vsize_ = int(vsize) * 1024 * 1024
rsize_ = int(rsize) * 1024 * 1024

q_readers_ = int(q_readers)
q_writers_ = int(q_writers)
a_readers_ = int(a_readers)
a_writers_ = int(a_writers)
net_in_    = int((float(net_in)  - float(_net_in))  / (now - float(ts)))
net_out_   = int((float(net_out) - float(_net_out)) / (now - float(ts)))
conn_      = int(conn)
connAvail_ = int(connAvail)

setName_   = 'N/A'
isMaster_  = 'N/A'
secondary_ = 'N/A'

if 'repl' in res:
    setName_   = res['repl']['setName']
    isMaster_  = str(res['repl']['ismaster'])
    secondary_ = str(res['repl']['secondary'])

# Save opcounters
try:
    if os_ == "Windows":
        temp_dir = os.environ["TEMP"]
        # f = open("C:\\Users\\hammer\\" + mongohost + "-configsvr-opcounters", 'w')
        f = open(temp_dir + "\\" + mongohost + "-configsvr-opcounters", 'w')
    elif os_ == "Linux":
        f = open("/tmp/" + mongohost + "-configsvr-opcounters", 'w')

    f.write(str(int(now)) + ' ' + str(insert) + ' ' + str(query) + ' ' + str(update) + ' ' + \
            str(delete) + ' ' + str(getmore) + ' ' + str(command) + ' ' + str(commandRepl) + ' ' + \
            str(net_in) + ' ' + str(net_out) + ' ' + str(flushes)
            )
    f.close()
except Exception as e:
    print("Can't update stats!")
    print(e)
    sys.exit(1)

mongos_total_ops = int(insert_) + int(query_) + int(update_) + int(delete_) + int(getmore_) + int(command_)
print(insert_, query_, update_, delete_, getmore_, command_, commandRepl_, net_in_, net_out_, flushes_, setName_,
      isMaster_, secondary_, dirty_, used_)

# mongos_total_ops=0
err = 'OK'
state = 1

packet = [ZabbixMetric(zbhost, 'configsvr_state',  state),
          ZabbixMetric(zbhost, 'configsvr_errstr', err)]

packet.append(ZabbixMetric(zbhost, "configsvr_insert",      int(insert_)))
packet.append(ZabbixMetric(zbhost, "configsvr_query",       int(query_)))
packet.append(ZabbixMetric(zbhost, "configsvr_update",      int(update_)))
packet.append(ZabbixMetric(zbhost, "configsvr_delete",      int(delete_)))
packet.append(ZabbixMetric(zbhost, "configsvr_getmore",     int(getmore_)))
packet.append(ZabbixMetric(zbhost, "configsvr_command",     int(command_)))
packet.append(ZabbixMetric(zbhost, "configsvr_commandRepl", int(commandRepl_)))
packet.append(ZabbixMetric(zbhost, "configsvr_dirty",       dirty_))
packet.append(ZabbixMetric(zbhost, "configsvr_used",        used_))
packet.append(ZabbixMetric(zbhost, "configsvr_flushes",     int(flushes_)))
packet.append(ZabbixMetric(zbhost, "configsvr_vsize",       int(vsize_)))
packet.append(ZabbixMetric(zbhost, "configsvr_rsize",       int(rsize_)))
packet.append(ZabbixMetric(zbhost, "configsvr_qReaders",    int(q_readers_)))
packet.append(ZabbixMetric(zbhost, "configsvr_qWriters",    int(q_writers_)))
packet.append(ZabbixMetric(zbhost, "configsvr_aReaders",    int(a_readers_)))
packet.append(ZabbixMetric(zbhost, "configsvr_aWriters",    int(a_writers_)))
packet.append(ZabbixMetric(zbhost, "configsvr_netin",       int(net_in_)))
packet.append(ZabbixMetric(zbhost, "configsvr_netout",      int(net_out_)))
packet.append(ZabbixMetric(zbhost, "configsvr_conn",        int(conn_)))
packet.append(ZabbixMetric(zbhost, "configsvr_connAvail",   int(connAvail_)))

packet.append(ZabbixMetric(zbhost, "configsvr_setName",     setName_))
packet.append(ZabbixMetric(zbhost, "configsvr_isMaster",    isMaster_))
packet.append(ZabbixMetric(zbhost, "configsvr_secondary",   secondary_))

packet.append(ZabbixMetric(zbhost, "configsvr_total_ops",   int(mongos_total_ops)))

# testMode = test  --> Do not send the data
if testmode == "test":
    print("---------------------------------------")
    print("Don't send the data to Zabbix Server.!!")
    print("---------------------------------------")
    sys.exit(0)
else:
    try:
        t = ZabbixSender(zabbix_port=ZBPORT, zabbix_server=ZBSERVER).send(packet)
    except Exception as e:
        print("Can't send data to zabbix server")
        print(e)
        sys.exit(1)
    print(t)
