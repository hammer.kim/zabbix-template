#!/usr/bin/python
#
from __future__ import print_function
import sys, getopt
from pyzabbix import ZabbixMetric, ZabbixSender
from pymongo import MongoClient
import time
import subprocess
import re
import urllib
import platform
import os

"""
For mongos Server
"""

ZBSERVER = '127.0.0.1'
ZBPORT = 10051

os_ = platform.system()

testmode='real'

#for item in os.environ:
#    print("%s = %s" % (item, os.environ[item]))

#print(" TEMP = " + os.environ["TEMP"])

# for hadoop cluster
# 27017 : mongos
# 27020 : config server
# 27021 : shard server
# mongostat --port 27017 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27020 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27021 -u root -p aimiramm --authenticationDatabase admin --rowcount 1 --noheaders


# [mongo@hadoop001 ~]$  mongostat --port 27021 -u root -p aimiramm --authenticationDatabase admin --rowcount 1
# insert query  update delete   getmore command dirty  used  flushes vsize  res   qrw arw net_in  net_out conn       set  repl                time
# *0     *0     *0     *0       0       13|0    0.1%   5.0%  0       1.76G  354M  0|0 1|0 1.69k   74.1k   53   lwm2m_rs1   PRI  Jun 23 20:51:24.948


# for nngp cluster
# 27017 : mongos
# 27022 : config server
# 27033 : shard server
# mongostat --port 27017 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27022 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders
# mongostat --port 27033 -u admin -p admin --authenticationDatabase admin --rowcount 1 --noheaders

# [mdm@nngp001 ~]$ mongostat --host 172.16.10.170 --port 27017 -u admin -p admin  --authenticationDatabase admin --rowcount 1
# insert query update delete getmore command flushes mapped vsize   res   faults  qrw  arw   net_in  net_out  conn                time
# *0    *0     *0     *0     0       1|0       0     0B     324M    24.0M      0  0|0  0|0   370b    12.1k    2     Jun 23 20:46:52.481

# [mdm@nngp001 ~]$ mongostat --host 172.16.10.170 --port 27022 -u admin -p admin  --authenticationDatabase admin --rowcount 1
# insert query update delete getmore command dirty  used flushes  vsize    res  qrw  arw   net_in  net_out  conn       set  repl                 time
# *0     1     *0     *0       0     3|0     0.0%   0.1%       0  2.00G  179M   0|0  1|0   896b    39.3k    20   rptconfig  PRI   Jun 23 20:48:44.853

# [mdm@nngp001 ~]$ mongostat --host 172.16.10.170 --port 27033 -u admin -p admin  --authenticationDatabase admin --rowcount 1
# insert query  update delete  getmore command dirty  used flushes  vsize   res     qrw  arw   net_in  net_out  conn                time
# *0     *0     *0     *0      0       4|0     0.0%   0.0%       0  1.61G   84.0M   0|0  1|0   832b    147k     7    Jun 23 20:49:41.751

# E:/GitLab/zabbix-template/mongodb-4.0.x/configsvr.py -h nngp001   -n nngp001 -u admin -s admin -p 27017 -m test
# E:/GitLab/zabbix-template/mongodb-4.0.x/configsvr.py -h hadoop001 -n hadoop001 -u root -s aimiramm -p 27017 -m test

try:
    opts, args = getopt.getopt(sys.argv[1:],"h:n:p:u:s:m:")
except getopt.GetoptError:
    print ("Usage:\nmongos.py -h <hostname or ip mongod is listening to> -n <hostname in zabbix> -u <user> -s <secret pass> -p <mongod port> -m <test mode>")
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        mongohost = arg
    elif opt == '-n':
        zbhost = arg
    elif opt == '-p':
        mongoport = arg
    elif opt == '-u':
        muser = arg
    elif opt == '-s':
        mpass = arg
    elif opt == '-m':
        testmode = arg

# faced a strange problem - zabbix server add a space character to parameter passed to script
# and spend a hour to catch the bug :(
mongohost = mongohost.rstrip()
zbhost    = zbhost.rstrip()


# print("DEBUG: STR: " + arg)
# print("DEBUG: RES: " + res)
# print("DEBUG: ERR: " + err + str(len(err)))

state = 0

# Read saved opcounters from previous check
try:
    if os_ == "Windows":
        temp_dir = os.environ["TEMP"]
#        f = open("C:\\Users\\hammer\\" + mongohost + "-mongos-opcounters")
        f = open(temp_dir + "\\" + mongohost + "-mongos-opcounters")
    elif os_ == "Linux":
        f = open("/tmp/" + mongohost + "-mongos-opcounters")

    s = f.read()
    f.close()
    ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _flushes, _vsize, _rsize, _q_readers, _q_writers, _a_readers, _a_writers, \
    _net_in, _net_out, _conn, _connAvail = s.split(" ")
except Exception as e:
    ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _flushes, _vsize, _rsize, _q_readers, _q_writers, _a_readers, _a_writers,  _net_in, _net_out, _conn, _connAvail = [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    print(e)

print("History opcounters")
print(ts, _insert, _query, _update, _delete, _getmore, _command, _commandRepl, _flushes, _vsize, _rsize, _q_readers, _q_writers, _a_readers, _a_writers, _net_in, _net_out, _conn, _connAvail)

# Get serverStatus stats
try:
    mo = MongoClient('mongodb://' + muser + ':' + urllib.quote(mpass) + '@' + mongohost + ':' + mongoport + '/admin', connectTimeoutMS=5000)
except Exception as e:
    print ('Can\'t connect to '+mongohost)
    print ("ERROR:", e)
    sys.exit(1)

res = mo.admin.command('serverStatus' )
now = time.time()

insert  = int((float(res['opcounters']['insert' ])))
query   = int((float(res['opcounters']['query'  ])))
update  = int((float(res['opcounters']['update' ])))
delete  = int((float(res['opcounters']['delete' ])))
getmore = int((float(res['opcounters']['getmore'])))
command = int((float(res['opcounters']['command'])))

# not in monogos
commandRepl = 0
# not in monogos
flushes = 0

vsize= int((float(res['mem']['virtual'])))
rsize= int((float(res['mem']['resident'])))

# not in monogos
q_readers = 0
q_writers = 0
a_readers = 0
a_writers = 0

# _q_readers= int((float(res['globalLock']['currentQueue']['readers'])))
# _q_writers= int((float(res['globalLock']['currentQueue']['writers'])))
# _a_readers= int((float(res['globalLock']['activeClients']['readers'])))
# _a_writers= int((float(res['globalLock']['activeClients']['writers'])))

net_in= int((float(res['network']['bytesIn'])))
net_out= int((float(res['network']['bytesOut'])))

conn      = int((float(res['connections']['current'])))
connAvail = int((float(res['connections']['available'])))

insert_      = int((float(insert)       - float(_insert))      / (now - float(ts)))
query_       = int((float(query)        - float(_query))       / (now - float(ts)))
update_      = int((float(update)       - float(_update))      / (now - float(ts)))
delete_      = int((float(delete)       - float(_delete))      / (now - float(ts)))
getmore_     = int((float(getmore)      - float(_getmore))     / (now - float(ts)))
command_     = int((float(command)      - float(_command))     / (now - float(ts)))
commandRepl_ =  int((float(commandRepl) - float(_commandRepl)) / (now - float(ts)))

flushes_ =  int(flushes)

# unit is Megabytes, so convert to bytes
vsize_   =  int(vsize) * 1024 * 1024
rsize_   =  int(rsize) * 1024 * 1024

q_readers_ = int(q_readers)
q_writers_ = int(q_writers)
a_readers_ = int(a_readers)
a_writers_ = int(a_writers)
net_in_    = int((float(net_in) - float(_net_in))/((now - float(ts))))
net_out_   = int((float(net_out) - float(_net_out))/((now - float(ts))))
conn_      = int(conn)
connAvail_ = int(connAvail)
# Save opcounters
try:
    if os_ == "Windows":
        temp_dir = os.environ["TEMP"]
        #f = open("C:\\Users\\hammer\\" + mongohost + "-mongos-opcounters", 'w')
        f = open(temp_dir + "\\" + mongohost + "-mongos-opcounters", 'w')
    elif os_ == "Linux":
        f = open("/tmp/" + mongohost + "-mongos-opcounters", 'w')

    f.write(str(int(now)) + ' ' + str(insert) + ' ' + str(query) + ' ' + str(update) + ' ' + \
            str(delete) + ' ' + str(getmore) + ' ' + str(command) + ' ' + str(commandRepl) + ' ' +\
            str(flushes) + ' ' + str(vsize) + ' ' + str(rsize) + ' ' +\
            str(q_readers) + ' ' + str(q_writers) + ' ' + str(a_readers) + ' ' + str(a_writers)  + ' ' +\
            str(net_in) + ' ' + str(net_out) + ' ' + str(conn)+ ' ' + str(connAvail)
            )
    f.close()
except Exception as e:
    print("Can't update stats!")
    print(e)
    sys.exit(1)

mongos_total_ops = int(insert_) + int(query_) + int(update_) + int(delete_) + int(getmore_) + int(command_)
print( insert_, query_, update_, delete_, getmore_, command_, commandRepl_, flushes_, vsize_, rsize_, q_readers_, q_writers_, a_readers_, a_writers_, net_in_, net_out_, conn_, connAvail_)

#mongos_total_ops=0
err = 'OK'
state = 1

packet = [ ZabbixMetric(zbhost, 'mongos_state', state),
           ZabbixMetric(zbhost, 'mongos_errstr', err) ]

packet.append(ZabbixMetric(zbhost, "mongos_insert",      int(insert_          )))
packet.append(ZabbixMetric(zbhost, "mongos_query",       int(query_           )))
packet.append(ZabbixMetric(zbhost, "mongos_update",      int(update_          )))
packet.append(ZabbixMetric(zbhost, "mongos_delete",      int(delete_          )))
packet.append(ZabbixMetric(zbhost, "mongos_getmore",     int(getmore_         )))
packet.append(ZabbixMetric(zbhost, "mongos_command",     int(command_         )))
packet.append(ZabbixMetric(zbhost, "mongos_commandRepl", int(commandRepl_     )))
packet.append(ZabbixMetric(zbhost, "mongos_flushes",     int(flushes_         )))
packet.append(ZabbixMetric(zbhost, "mongos_vsize",       int(vsize_           )))
packet.append(ZabbixMetric(zbhost, "mongos_rsize",       int(rsize_           )))
packet.append(ZabbixMetric(zbhost, "mongos_qReaders",    int(q_readers_       )))
packet.append(ZabbixMetric(zbhost, "mongos_qWriters",    int(q_writers_       )))
packet.append(ZabbixMetric(zbhost, "mongos_aReaders",    int(a_readers_       )))
packet.append(ZabbixMetric(zbhost, "mongos_aWriters",    int(a_writers_       )))
packet.append(ZabbixMetric(zbhost, "mongos_netin",       int(net_in_          )))
packet.append(ZabbixMetric(zbhost, "mongos_netout",      int(net_out_         )))
packet.append(ZabbixMetric(zbhost, "mongos_conn",        int(conn_            )))
packet.append(ZabbixMetric(zbhost, "mongos_connAvail",   int(connAvail_       )))
packet.append(ZabbixMetric(zbhost, "mongos_total_ops",   int(mongos_total_ops )))

# testMode = test  --> Do not send the data
if testmode == "test":
    print("---------------------------------------")
    print("Don't send the data to Zabbix Server.!!")
    print("---------------------------------------")
    sys.exit(0)
else:
    try:
        t = ZabbixSender(zabbix_port=ZBPORT, zabbix_server=ZBSERVER).send(packet)
    except Exception as e:
        print("Can't send data to zabbix server")
        print(e)
        sys.exit(1)
    print(t)